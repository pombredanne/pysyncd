#!/gm/bin/python
"""
 * Copyright 2012, Scott Douglass <scott@swdouglass.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * on the World Wide Web for more details:
 * http://www.fsf.org/licensing/licenses/gpl.txt
"""

"""
https://github.com/seb-m/pyinotify
http://docs.python.org/library/multiprocessing.html
"""

import os
import sys
import time
import subprocess
import signal
from pyinotify import ProcessEvent, WatchManager, ThreadedNotifier, IN_MODIFY, IN_DELETE
from getopt import getopt, GetoptError
from multiprocessing import Process, Queue

class EventHandler(ProcessEvent):
    def __init__ (self, queue):
        self.queue = queue
        self.events = 0
    def _enqueue (self, event):
        self.events = self.events + 1
        #print "Events: ", self.events
        self.queue.put(event.path)
        #print "Queue size: ", self.queue.qsize()
    def process_IN_MODIFY(self, event):
        print "IN_MODIFY"
        self._enqueue(event)
    def process_IN_DELETE(self, event):
        print "IN_DELETE"
        self._enqueue(event)

def rsync(queue, target):
    try: 
        while True:
            path = queue.get()
            #print path
            if os.path.exists(path):
                status = subprocess.call(["rsync", "-avHXASR", "--delete",  path, target])
                #print "rsync exit: ", status, "\n"
                #if status != 0:
                #    print "WARNING: rsync problem."
    except KeyboardInterrupt:
        pass

def usage():
    print "python pysyncd.py -s source -t target"
    sys.exit(1)

def exitHandler(signum, frame):
    sys.exit(signum)

def main():
    source = ''
    target = ''

    if len(sys.argv[1:]) == 0:
        usage()

    try:
        opts, args = getopt(sys.argv[1:], "hs:t:", ["help", "source=","target="])
    except GetoptError, err:
        print str(err)
        usage()
    
    for o, a in opts:
        if o in ("-s","--source"):
            source = a
        elif o in ("-t","--target"):
            target = a
        elif o in ("-h", "--help"):
            usage()
        else:
            assert False, "invalid option"

    signal.signal(signal.SIGTERM, exitHandler)

    queue = Queue(50)

    process = Process(target=rsync, args=(queue,target))
    process.daemon = True
    process.name = "rsync"
    process.start()

    mask = IN_MODIFY|IN_DELETE
    wm = WatchManager()
    wm.add_watch(source, mask, rec=True)
    notifier = ThreadedNotifier(wm, EventHandler(queue,))
    notifier.daemon = True
    notifier.start()

    try:
        while True: time.sleep(100)
    except (KeyboardInterrupt, SystemExit):
       notifier.stop()
       process.terminate()
       if queue.qsize() > 0:
           print "WARNING: Exit with queue size: ", queue.qsize()

if __name__ == "__main__":
    main()

